﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum State
    {
        Small,
        Big,
        Fire
    }

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Sprite standSprite;
    [SerializeField] Sprite jumpSprite;
    [SerializeField] Sprite[] walkSprites;
    [SerializeField] Rigidbody2D rigidbody;
    [SerializeField] float walkSpeed;
    [SerializeField] float runSpeed;
    [SerializeField] float jump;
    [SerializeField] GameObject fireBall;
    public State state = State.Small;
    public float godTimer = 0f;
    bool isGround = false;
    List<ContactPoint2D> contacts = new List<ContactPoint2D>();
    float fireTimer = 0f;
    float spriteTimer = 0f;
    int spriteIndex = 0;

    void Update()
    {
        Camera.main.transform.position = new Vector3(this.transform.position.x, 0f, -10f);

        if (godTimer <= 0f)
        {
            this.gameObject.layer = LayerMask.NameToLayer("Default");
            spriteRenderer.enabled = true;
        }
        else
        {
            this.gameObject.layer = LayerMask.NameToLayer("God");
            godTimer -= Time.deltaTime;
            spriteRenderer.enabled = !spriteRenderer.enabled;
        }

        isGround = false;

        rigidbody.GetContacts(contacts);

        for (int i = 0; i < contacts.Count; i++)
        {
            if (contacts[i].normal.y > 0.7f)
            {
                isGround = true;
            }
        }

        Vector2 velocity = rigidbody.velocity;

        float speed = walkSpeed;
        if (Input.GetKey(KeyCode.X))
        {
            speed = runSpeed;
        }

        fireTimer += Time.deltaTime;
        if (state == State.Fire && fireTimer > 0.5f && Input.GetKeyDown(KeyCode.X))
        {
            fireTimer = 0f;

            GameObject clone = Instantiate(fireBall);
            if (spriteRenderer.flipX)
            {
                clone.transform.position = this.transform.position + new Vector3(-1f, 0f, 0f);
                clone.GetComponent<Rigidbody2D>().velocity = new Vector2(-10f, 0f);
            }
            else
            {
                clone.transform.position = this.transform.position + new Vector3(1f, 0f, 0f);
                clone.GetComponent<Rigidbody2D>().velocity = new Vector2(10f, 0f);
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            spriteRenderer.flipX = true;
            velocity.x -= speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            spriteRenderer.flipX = false;
            velocity.x += speed * Time.deltaTime;
        }

        if (isGround && Input.GetKeyDown(KeyCode.Z))
        {
            velocity.y = jump;
        }

        rigidbody.velocity = velocity;

        if (isGround)
        {
            if (Mathf.Abs(velocity.x) < 0.1f)
            {
                spriteRenderer.sprite = standSprite;
            }
            else
            {
                spriteTimer += Mathf.Abs(velocity.x);
                if (spriteTimer > 20f)
                {
                    spriteTimer -= 20f;
                    spriteIndex++;
                    spriteIndex = spriteIndex % walkSprites.Length;
                }
                spriteRenderer.sprite = walkSprites[spriteIndex];
            }
        }
        else
        {
            spriteRenderer.sprite = jumpSprite;
        }
    }

    void FixedUpdate()
    {
        Vector2 velocity = rigidbody.velocity;

        velocity.x *= 0.9f;

        rigidbody.velocity = velocity;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Item1")
        {
            SetState(State.Big);
            Destroy(collision.gameObject);
        }

        if (collision.collider.tag == "Item2")
        {
            SetState(State.Fire);
            Destroy(collision.gameObject);
        }
    }

    public void SetState(State state)
    {
        this.state = state;
        switch (state)
        {
            case State.Small:
                this.transform.localScale = new Vector3(1f, 1f, 1f);
                spriteRenderer.color = Color.white;
                break;
            case State.Big:
                this.transform.localScale = new Vector3(1f, 2f, 1f);
                spriteRenderer.color = Color.white;
                break;
            case State.Fire:
                this.transform.localScale = new Vector3(1f, 2f, 1f);
                spriteRenderer.color = Color.red;
                break;
        }
    }

    void OnDrawGizmos()
    {
        for (int i = 0; i < contacts.Count; i++)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(contacts[i].point, 0.1f);
        }
    }
}
