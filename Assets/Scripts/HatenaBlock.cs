﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatenaBlock : MonoBehaviour
{
    [SerializeField] GameObject item;
    [SerializeField] Sprite openedSprite;
    bool isOpened = false;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (isOpened)
            return;

        if (collision.collider.tag == "Player")
        {
            for (int i = 0; i < collision.contacts.Length; i++)
            {
                if (collision.contacts[i].normal.y > 0.7f)
                {
                    isOpened = true;
                    GetComponent<SpriteRenderer>().sprite = openedSprite;

                    if (item == null)
                        return;

                    StartCoroutine(Open());
                    return;
                }
            }
        }
    }

    IEnumerator Open()
    {
        GameObject clone = Instantiate(item);

        for (int i = 10; i < 60; i++)
        {
            clone.transform.position = this.transform.position + new Vector3(0f, 1f / 60 * i);
            yield return null;
        }
    }
}
