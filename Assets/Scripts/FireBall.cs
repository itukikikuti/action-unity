﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    void Start()
    {
        Destroy(this.gameObject, 10f);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }
        else
        {
            for (int i = 0; i < collision.contacts.Length; i++)
            {
                if (collision.contacts[i].normal.x > 0.7f)
                {
                    Destroy(this.gameObject);
                }
                if (collision.contacts[i].normal.x < -0.7f)
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
