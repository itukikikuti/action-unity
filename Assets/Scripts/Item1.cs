﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item1 : MonoBehaviour
{
    [SerializeField] Rigidbody2D rigidbody;
    [SerializeField] float speed;

    void FixedUpdate()
    {
        Vector2 velocity = rigidbody.velocity;

        velocity.x = speed;

        rigidbody.velocity = velocity;
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        for (int i = 0; i < collision.contacts.Length; i++)
        {
            if (collision.contacts[i].normal.x > 0.7f)
            {
                speed = Mathf.Abs(speed);
            }
            if (collision.contacts[i].normal.x < -0.7f)
            {
                speed = -Mathf.Abs(speed);
            }
        }
    }
}
