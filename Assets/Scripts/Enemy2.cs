﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy2 : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Rigidbody2D rigidbody;
    [SerializeField] float speed;
    float spriteTimer = 0f;

    void Update()
    {
        Vector2 velocity = rigidbody.velocity;

        velocity.x += speed;
        velocity.x *= 0.9f;

        rigidbody.velocity = velocity;

        spriteTimer += Time.deltaTime;
        if (spriteTimer > 0.3f)
        {
            spriteTimer = 0f;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        for (int i = 0; i < collision.contacts.Length; i++)
        {
            if (collision.contacts[i].collider.tag == "Player")
            {
                Player player = collision.gameObject.GetComponent<Player>();
                if (player.state == Player.State.Small)
                {
                    SceneManager.LoadScene("Title");
                }
                else
                {
                    player.SetState(Player.State.Small);
                    player.godTimer = 1f;
                }
            }
            else
            {
                if (collision.contacts[i].normal.x > 0.5f)
                {
                    speed = Mathf.Abs(speed);
                }
                if (collision.contacts[i].normal.x < -0.5f)
                {
                    speed = -Mathf.Abs(speed);
                }
            }
        }
    }
}
