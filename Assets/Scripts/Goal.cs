﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [SerializeField] string nextScene;

    void Update()
    {
        this.transform.eulerAngles += new Vector3(0f, 3f, 0f);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
            SceneManager.LoadScene(nextScene);
    }
}
