﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy1 : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Rigidbody2D rigidbody;
    [SerializeField] float speed;
    float spriteTimer = 0f;

    void Update()
    {
        Vector2 velocity = rigidbody.velocity;

        velocity.x += speed;
        velocity.x *= 0.9f;

        rigidbody.velocity = velocity;

        spriteTimer += Time.deltaTime;
        if (spriteTimer > 0.3f)
        {
            spriteTimer = 0f;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        for (int i = 0; i < collision.contacts.Length; i++)
        {
            if (collision.contacts[i].collider.tag == "Player")
            {
                if (collision.contacts[i].normal.y < -0.5f)
                {
                    this.transform.position -= new Vector3(0f, 0.4f, 0f);
                    this.transform.localScale = new Vector3(1f, 0.25f, 1f);
                    collision.contacts[i].rigidbody.velocity = new Vector2(collision.contacts[i].rigidbody.velocity.x, 4f);
                    rigidbody.simulated = false;
                    Destroy(this);
                    Destroy(this.gameObject, 3f);
                    return;
                }
                else
                {
                    Player player = collision.gameObject.GetComponent<Player>();
                    if (player.state == Player.State.Small)
                    {
                        SceneManager.LoadScene("Title");
                    }
                    else
                    {
                        player.SetState(Player.State.Small);
                        player.godTimer = 1f;
                    }
                }
            }
            else
            {
                if (collision.contacts[i].normal.x > 0.5f)
                {
                    speed = Mathf.Abs(speed);
                }
                if (collision.contacts[i].normal.x < -0.5f)
                {
                    speed = -Mathf.Abs(speed);
                }
            }
        }
    }
}
